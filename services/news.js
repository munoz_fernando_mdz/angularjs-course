angular.module('myApp-NewsService', [])
.factory('NewsServices', ['$http', function ($http) {

    var data = {};

    data.push = function (item) {
        var items = this.list();
        items.push(item);

        window.localStorage.setItem('news', JSON.stringify(items));
    };

    data.list = function () {
        var items = window.localStorage.getItem('news') || '[]';
        return JSON.parse(items);
    };

    data.get = function(i){
        var items = this.list();
        return items[i];
    };

    return data;

}]);