angular.module('myApp-TreeService', [])
.factory('TreeServices', ['$http', function ($http) {

    var data = {};

    data.list = function () {

        var account = 'jarvistecstorage';
        var url = 'https://' + account + '.file.core.windows.net/?restype=service&comp=properties';
        var date = (new Date()).toUTCString();
        var auth = 'SharedKey ' + account + ':haL0DiWlsBHwBC04NFxhjFSNrQeqq0BjDglBZy2qE3AJaI8+bEiNJqeOQ2pOhkJnmTZHMSL9Rnm8rXoHXPZjJw==';

        console.log(date);

        $http({
            method: 'PUT',
            url: url,
            headers: {
                'Authorization': auth,
                'x-ms-date': date
            }
        })
        .then(
            function successCallback(data) {
                console.log('data', data);
            },
            function errorCallback(error) {
                console.log('error', error);
            }
        );

    };

    return data;

}]);