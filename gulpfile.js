// https://github.com/ChrisLTD/html_template/blob/master/gulpfile.js

const gulp      = require('gulp');
const plumber   = require('gulp-plumber');
const sass      = require('gulp-sass');
const webserver = require('gulp-webserver');
const opn       = require('opn');
const swig      = require('gulp-swig');
const notify    = require('gulp-notify');
const gutil     = require('gulp-util');

const sourcePaths = {
    scripts: [
        './main.js',
        './pages/**/*.js',
        './directives/**/*.js'
    ],
    templates: [
        './**/*.html',
        '~./node_modules/**/*.html'
    ],
    style: './**/*.scss'
};

const server = {
    host: 'localhost',
    port: '8002'
}

/* ==== Errors Handlers
 ============================================ */

function taskErrorHandler(err) {
    notify.onError({
        title: "Gulp error in " + err.plugin,
        message:  err.toString()
    })(err);

    // play a sound once
    gutil.beep();
    this.emit('end');
};

/* ==== Tasks
 ============================================ */

gulp.task('templates', () => {
    return gulp.src( sourcePaths.templates )
        .pipe(plumber())
        .pipe(swig({
            defaults: {
                cache: false
            }
        }));
});

gulp.task('sass', () => {
    return gulp.src('./style.scss')
        .pipe(plumber({ errorHandler: taskErrorHandler}))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest( './' ));
});

gulp.task('scripts', () => {
    return gulp.src( sourcePaths.scripts )
        .pipe(plumber())
        .pipe(swig({
            defaults: {
                cache: false
            }
        }));
});

gulp.task('webserver', () => {
    gulp.src( '.' )
        .pipe(webserver({
            host:             server.host,
            port:             server.port,
            livereload:       true,
            directoryListing: false
        }));
});

gulp.task('openbrowser', () => {
    opn(`http://${server.host}:${server.port}`);
});

gulp.task('watch', () => {
    gulp.watch(sourcePaths.scripts, ['scripts']);
    gulp.watch(sourcePaths.templates, ['templates']);
    gulp.watch(sourcePaths.style, ['sass']);
});

gulp.task('build', ['sass']);
gulp.task('default', ['build', 'webserver', 'watch', 'openbrowser']);