angular.module('FileManager-Directive', [])
.directive('fileManager', function(FileManagerService) {

    return {
        restrict: 'E',
        templateUrl: '/directives/file-manager/view.html',
        scope: {
            url: '@url'
        },
        link: function(scope, element){
            scope.items = [];
            scope.actual = FileManagerService.actual;

            var fc = function (data, action) {
                scope.items = data;

                if(action == 'list'){
                    setTimeout(function(){
                        var scroll = element[0].querySelector('.explorer');
                        scroll.scrollTo(scroll.scrollWidth, 0);
                    }, 150);
                }
            };

            FileManagerService.load(scope.url, fc);

            /* ------------------------------------------------------ */

            scope.dialogActive = false;
            scope.renameActive = false;
            scope.removeActive = false;

            scope.onConfirmRename = function(){
                scope.dialogActive = false;
                scope.renameActive = false;
            };

            scope.onCancelRename = function(){
                scope.dialogActive = false;
                scope.renameActive = false;
            };

            scope.onConfirmRemove = function(){
                scope.dialogActive = false;
                scope.removeActive = false;
            };

            scope.onCancelRemove = function(){
                scope.dialogActive = false;
                scope.removeActive = false;
            };

            scope.onRemove = function(){
                scope.dialogActive = true;
                scope.renameActive = true;
            };

            scope.onRename = function(){
                scope.dialogActive = true;
                scope.removeActive = true;
            };

            scope.view = function(){
                //stViewer.open('http://www.intrawallpaper.com/static/images/farcry_3-1280x720.jpg', 'image');
                //stViewer.open('http://coderthemes.com/ubold_2.4/b4/mdlight/assets/images/users/avatar-1.jpg', 'image');
                stViewer.open('http://www.frm.utn.edu.ar/archivos/oferta%20educativa/ing_sistemas/planificaciones/UTN_FRM_ISI_Planificacin_lgebra_y_Geometra_Analtica_2016_3.pdf', 'pdf');
            };
        }
    };
});
