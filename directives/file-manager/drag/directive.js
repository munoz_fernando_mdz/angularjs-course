angular.module('FileManagerDrag-Directive', [])
.directive('fileManagerDrag', function () {

    function uploadDragEnter(e){
        this.setAttribute('draging', true);
    }

    function uploadDragLeave(e){
        this.setAttribute('draging', false);
    }

    function uploadDragOver(e){
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    }

    function uploadFileSelect(e) {
        e.stopPropagation();
        e.preventDefault();
        this.setAttribute('draging', false);

        var files = e.dataTransfer ? e.dataTransfer.files: e.target.files;

        for (var i = 0, file; file = files[i]; ++i) {
            var reader = new FileReader();
            reader.onload = (function(file) {
                return function(e) {

                    console.log(file);

                    // Data handling (just a basic example):
                    // [object File] produces an empty object on the model
                    // why we copy the properties to an object containing
                    // the Filereader base64 data from e.target.result
                    var data={
                        data:e.target.result,
                        dataSize: e.target.result.length
                    };
                    for(var p in file){ data[p] = file[p] }


                }
            })(file);
            reader.readAsDataURL(file);
        }
    }

    return {
        restrict: 'A',
        link: function (scope, element) {
            e = element[0];
            e.dragable = true;
            e.addEventListener('dragenter', uploadDragEnter, false);
            e.addEventListener('dragover', uploadDragOver, false);
            e.addEventListener('dragleave', uploadDragLeave, false);
            e.addEventListener('drop', uploadFileSelect, false);
            e.addEventListener('change', uploadFileSelect, false);
        }
    };
});