(function() {
    'use strict';

    angular.module('File-Manager', [
        'FileManager-Directive',
        'FileManagerHierarchy-Directive',
        'FileManagerDrag-Directive',
        'FileManager-Service'
    ]);
})();