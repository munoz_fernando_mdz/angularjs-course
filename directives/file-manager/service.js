angular.module('FileManager-Service', [])
.factory('FileManagerService', function ($http) {

    /* Variables */

    var url;
    var data = {};
    var callback;

    /* Object */

    data.data = [];
    data.actual = {
        path: ['/'],
        position: {h: -1, i: -1}
    };

    data.load = function(_url, _callback){
        callback = _callback;
        url = _url;
        this.list('/');
    };

    data.select = function(h, i){
        // remove old hierarchies
        while(typeof this.data[h+1] !== 'undefined')
            this.data.splice(h+1, 1);
        while(typeof this.actual.path[h+1] !== 'undefined')
            this.actual.path.splice(h+1, 1);

        // add name to path
        this.actual.path.push(this.data[h][i].name);

        // path
        var path = this.actual.path.join('/');
        path = path.substring(1);

        // list folder
        if(this.data[h][i].type == 'folder')
            this.list(path);

        // unselect siblings
        for(var j=0; j<this.data[h].length; j++)
            this.data[h][j].selected = false;

        // select
        this.data[h][i].selected = true;

        // current
        this.actual.position = {h: h, i: i};
    };

    /* Http */

    data.rename = function(name, path){
        return $http({
            method: 'POST',
            url: url + '?method=rename',
            data: {
                path: path, // path include file
                name: name // new name
            }
        });
    };

    data.delete = function(path){
        return $http({
            method: 'DELETE',
            url: url + '?method=delete',
            data: {
                path: path
            }
        });
    };

    data.upload = function(base64){
        return $http({
            method: 'PUT',
            url: url + '?method=upload',
            data: {
                base64: base64
            }
        });
    };

    data.list = function(path){
        $http({
            method: 'POST',
            url: url + '?method=list',
            data: {
                path: path
            }
        }).then(
            function(response){
                if(response.data.code == 200){
                    data.data.push(response.data.data);
                    callback(data.data, 'list');
                }
                else
                    console.log(response.message || 'Error');
            },
            function(err){
                console.log('Error 2', err);
            }
        );
    };

    data.createFolder = function(name, path){
        return $http({
            method: 'PUT',
            url: url + '?method=createFolder',
            data: {
                neme: name,
                path: path
            }
        });
    };

    /* Return */

    return data;

});