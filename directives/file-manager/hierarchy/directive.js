angular.module('FileManagerHierarchy-Directive', [])
.directive('fileManagerHierarchy', function(FileManagerService) {
    return {
        restrict: 'E',
        scope: {
            items: '=items',
            hierarchy: '=hierarchy',
            h: '=h',
            i: '=i'
        },
        link: function(scope){
            scope.select = function(j){
                FileManagerService.select(scope.hierarchy, j);
            };

            scope.iconType = function(type){
                var r = '';

                switch (type){
                    case 'folder':
                        r = 'folder_open';
                    break;
                    case 'image/png':
                    case 'image/gif':
                    case 'image/jpeg':
                        r = 'portrait';
                    break;
                    case 'application/pdf':
                        r = 'import_contacts';
                    break;
                    case 'video/mpeg':
                    case 'video/ogg':
                    case 'video/x-msvideo':
                    case 'video/webm':
                        r = 'slideshow';
                    break;
                    default:
                        r = 'insert_drive_file';
                }

                return r;
            };
        },
        templateUrl: '/directives/file-manager/hierarchy/view.html'
    };
});
