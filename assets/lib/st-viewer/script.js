var stViewer = function(){

    var viewer = {};

    var types = {
        'image': function(url){
            return '<img src="' + url + '" />';
        },
        'pdf': function(url){
            return '<embed src="' + url + '" width="600" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">';
        }
    };

    viewer.open = function(url, type){
        if(!url && !type){
            console.error('Error. We need params: url and type.');
            return;
        }

        this.url = url;
        this.type = type;

        var icons = '<div class="icons">'
        icons += '<i onclick="stViewer.download()" class="material-icons">cloud_download</i>';
        icons += '<i onclick="stViewer.toggleExpand()" class="expand material-icons">fullscreen</i>';
        icons += '<i onclick="stViewer.close()" class="close material-icons">close</i>';
        icons += '</div>';

        var modal = '<div id="st-viewer">';
        modal += '<div class="scroll"><div class="content">';
        modal += '<div class="title">Preview ' + icons + '</div>';
        modal += '<section>' + types[this.type](this.url) + '</section>';
        modal += '</div></div>';
        modal += '</div>';

        $('body').append(modal);
    };

    viewer.download = function(){
        window.open(this.url, '_blank');
    };

    viewer.toggleExpand = function(){
        $('#st-viewer').toggleClass('expand');
        $('#st-viewer .expand').text( ($('#st-viewer .expand').text().trim() == 'fullscreen') ? 'fullscreen_exit' : 'fullscreen' );
    };

    viewer.close = function(){
        $('#st-viewer').addClass('dying');
        setTimeout(function(){
            $('#st-viewer').remove();
        }, 251);
    };

    return viewer;

}();
