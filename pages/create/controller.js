angular.module('myApp-Create', [])
.controller('CreateCtrl', function($scope, $state, NewsServices){

    $scope.data = {
        title: 25,
        body: ''
    };

    $scope.save = function(){

        $scope.data.date = new Date();

        NewsServices.push($scope.data);

        $state.go('list');

    };

});