angular.module('myApp-View', [])
.controller('ViewCtrl', function($scope, $state, NewsServices){

    $scope.data = NewsServices.get($state.params.position);

});