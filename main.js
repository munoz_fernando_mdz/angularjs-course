var myApp = angular.module('myApp', [
    'myApp-Create',
    'myApp-List',
    'myApp-View',
    'myApp-Tree',

    'myApp-NewsService',
    'myApp-TreeService',

    'File-Manager',

    'ui.router'
]);

myApp.config(function($stateProvider, $urlRouterProvider) {

    // An array of state definitions
    var states = [
        {
            name: 'create',
            url: '/create',
            controller: "CreateCtrl",
            templateUrl: 'pages/create/view.html'
        },
        {
            name: 'list',
            url: '/list',
            controller: "ListCtrl",
            templateUrl: 'pages/list/view.html'
        },
        {
            name: 'tree',
            url: '/tree',
            controller: "TreeCtrl",
            templateUrl: 'pages/tree/view.html'
        },
        {
            name: 'view',
            url: '/view',
            controller: "ViewCtrl",
            templateUrl: 'pages/view/view.html',
            params: {
                position: null
            }
        }
    ];

    // Loop over the state definitions and register them
    states.forEach(function(state) {
        $stateProvider.state(state);
    });

    // Default page
    $urlRouterProvider.otherwise('/list');

});

myApp.run(function(){


});